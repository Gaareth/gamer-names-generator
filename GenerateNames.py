import os
import sys
import time
import math
import torch
import string
import random
import unicodedata
import torchvision
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import matplotlib.pyplot as plt
from torchvision import transforms
from torch.autograd import Variable
from PIL import Image
from os import listdir

all_letters = string.ascii_letters + " .,;'-"
n_letters = len(all_letters) + 1 # Plus EOS marker


def shuffleFile(filename):
    lines = open(filename).readlines()
    random.shuffle(lines)
    open(filename, 'w').writelines(lines)
    print("Shuffled File")


def onlyLong(filename, int = 7):
    lines = open(filename, encoding='utf-8').read().strip().split('\n')
    list =[]
    list = lines
    rlist = []
    for x in list:
        if len(x) > int:
            rlist.append(removeWhitespaces(x))
    return rlist


def removeWhitespaces(input):
    #intab = " "
    #outtab = ""
    #trantab = input.maketrans(intab, outtab)
    #return input.translate(trantab)
    return input.replace(" ", "")


def unicodeToAscii(s):
    return ''.join(
        c for c in unicodedata.normalize('NFD', s)
        if unicodedata.category(c) != 'Mn'
        and c in all_letters)


def readLines(filename):
    shuffleFile(filename)
    lines = open(filename, encoding='utf-8').read().strip().split('\n')
    return [unicodeToAscii(line) for line in lines]


def readLinesX(filename, y):
    shuffleFile(filename)
    with open(filename) as myfile:
        lines = [next(myfile) for x in range(y)]
    return [unicodeToAscii(line) for line in lines]

data = []
#shuffleFile("data/lang/data/gamingnames2.txt")
lines = readLines("data/lang/data/gamingnameslong.txt")
data = lines

#print("DataSet 1 : [{Size: ", len(onlyLong("data/lang/data/gamingnames2.txt",12)), "}, {Data: ", onlyLong("data/lang/data/gamingnames2.txt",14), "}]")

#for n in onlyLong("data/lang/data/gamingnames2.txt",14):
#   open("data/lang/data/gamingnameslong.txt", 'a').writelines(n + "\n")

n_data= len(data)


#print("DataSet 2 : [{Size: ", n_data, "}, {Data: ", data, "}]")


class RNN(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(RNN, self).__init__()
        self.hidden_size = hidden_size

        self.i2h = nn.Linear(input_size + hidden_size, hidden_size)
        self.i2o = nn.Linear(input_size + hidden_size, output_size)
        self.o2o = nn.Linear(hidden_size + output_size, output_size)
        self.dropout = nn.Dropout(0.3)
        self.softmax = nn.LogSoftmax(dim=1)

    def forward(self, input, hidden):
        input_combined = torch.cat((input, hidden), 1)
        hidden = self.i2h(input_combined)
        output = self.i2o(input_combined)
        output_combined = torch.cat((hidden, output), 1)
        output = self.o2o(output_combined)
        output = self.dropout(output)
        output = self.softmax(output)
        return output, hidden

    def initHidden(self):
        return torch.zeros(1, self.hidden_size)


rnn = RNN(n_letters, 128, n_letters)
#rnn.cuda()


# One-hot matrix of  string
def inputTensor(line):
    tensor = torch.zeros(len(line), 1, n_letters)
    for li in range(len(line)):
        letter = line[li]
        tensor[li][0][all_letters.find(letter)] = 1
    return tensor


# LongTensor of label
def targetTensor(line):
    letter_indexes = [all_letters.find(line[li]) for li in range(1, len(line))]
    letter_indexes.append(n_letters - 1) # EOS
    return torch.LongTensor(letter_indexes)


def randomChoice(l):
    return l[random.randint(0, len(l) - 1)]


def randomTrainingPair():
    name = random.choice(data)
    return name


def randomTrainingExample():
    name = randomTrainingPair()
    input_line_tensor = inputTensor(name)
    target_line_tensor = targetTensor(name)
    return input_line_tensor, target_line_tensor


####################TRAIN##############################

criterion = nn.NLLLoss()

learning_rate = 0.0005


def train(input_line_tensor, target_line_tensor):
    target_line_tensor.unsqueeze_(-1)
    hidden = rnn.initHidden()

    rnn.zero_grad()

    loss = 0

    for i in range(input_line_tensor.size(0)):
        output, hidden = rnn(input_line_tensor[i], hidden)
        l = criterion(output, target_line_tensor[i])
        loss += l

    loss.backward()

    for p in rnn.parameters():
        p.data.add_(-learning_rate, p.grad.data)

    return output, loss.item() / input_line_tensor.size(0)


def timeSince(since):
    now = time.time()
    s = now - since
    m = math.floor(s / 60)
    s -= m * 60
    return '%dm %ds' % (m, s)


def trainNetwork(Epoch):
    n_iters = Epoch
    print_every = 100
    plot_every = 500
    all_losses = []
    total_loss = 0  

    start = time.time()

    for iter in range(1, n_iters + 1):
        output, loss = train(*randomTrainingExample())
        total_loss += loss

        if iter % print_every == 0:
            print('(%s) (Iterations: %d Percentage: %d%%) Loss: %.4f' % (timeSince(start), iter, iter / n_iters * 100, loss))

        if iter % plot_every == 0:
            all_losses.append(total_loss / plot_every)
            total_loss = 0


def sample(start_letter='A'):


    # rnn.eval()
    max_length = 20
    with torch.no_grad():
        try:
            rnn = torch.load("SaveFiles/GenerateNames.pt")
        except:
            print("No Network Found! Please Train First")
            quit()
       # print("Loaded Network!")

        input = inputTensor(start_letter)
        hidden = rnn.initHidden()

        output_name = start_letter

        for i in range(max_length):
            output, hidden = rnn(input[0], hidden)
            topv, topi = output.topk(1)
            topi = topi[0][0]
            if topi == n_letters - 1:
                break
            else:
                letter = all_letters[topi]
                output_name += letter
            input = inputTensor(letter)

        return output_name


def samples(r, start_letters='ABC'):
    start_letters = ''.join([random.choice(all_letters) for x in range(r)])

    for start_letter in start_letters:
        print("> ",sample(start_letter))






if __name__ == "__main__":
    try:
        if sys.argv[1] == "-p":
            samples(int(sys.argv[2]))

        elif sys.argv[1] == "-t":
            trainNetwork(int(sys.argv[2]))
            torch.save(rnn, "SaveFiles/GenerateNames.pt")
    except:
        print("\nSyntax: ", "-p [Amount], -t {Iterations]")

   

