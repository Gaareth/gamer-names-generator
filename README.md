# A Recurrent Neural Network which generates gamer names! #
My first attempt on a more useful Neural Network

## Usage ##
1. Download the file.
2. Make sure you have python version 3 and the pytorch module installed.
3. Open a terminal and type: 

* python generatenames -p [random char] for generating

  __OR__

* python generatenames -t for training